<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ar extends Model
{
    protected $fillable = [
        'account_receivable',
        'receipt',
        'cost'
    ];

    public function scopeFindByReceipt($query, $i)
    {
        $query->where('receipt', $i);
    }

    public function scopeFindByAr($query, $i)
    {
        $query->where('account_receivable', $i);
    }
}
