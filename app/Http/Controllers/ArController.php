<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreArRequest;
use App\Ar;

class ArController extends Controller
{
    
    public function index($offset)
    {
        $paginated = Ar::paginate($offset);
        
        return $paginated;
    }

    public function create()
    {
       
    }

    
    public function store(StoreArRequest $request)
    {
        $data = Ar::create($request->all());

        return response()->json($data, 201);
    }

    public function show($receipt)
    {
        $ar = Ar::where('receipt',$receipt)->get();
        
        return $ar;
    }

    public function paginated($offset, $receipt)
    {
        $paginated = Ar::findByReceipt($receipt)->paginate($offset);
        
        return $paginated;
    }

    public function delete($account_receivable)
    {
        $ar = Ar::findByAr($account_receivable)->first();
        $ar->delete();
        
        return response()->json(collect([
                'message' => 'sukses dihapus',
            ]), 204);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
