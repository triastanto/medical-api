<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use App\Http\Requests\StoreTransactionRequest;
use App\Transaction;
use App\Plafon;

class TransactionController extends Controller
{
    public function index()
    {
        // return Transaction::all();
    }

    public function show($registration_no)
    {
        $transactions = Transaction::ofRegisrationNo($registration_no)->get();

        return $transactions;
    }

    public function store(StoreTransactionRequest $request)
    {
        $transaction = Transaction::create($request->all()
            + [ 'updated_by' => Auth::user()->id ] 
        );

        return response()->json($transaction, 201);
    }

    public function update(Request $request, Transaction $transaction)
    {
        $transaction->update($request->all());

        return response()->json($transaction, 200);
    }

    public function delete($invoice_no, $registration_no, $location=null)
    {
        $check  = $transaction = Transaction::findByCompositeUniqueTwoField(
                    $invoice_no, $registration_no
                    )
                    ->first();
        
        $message = null;

        if(is_null($check))
        {
            $message = "Data tidak ditemukan";
        }
        else
        {
            if($location){
                $transaction = Transaction::findByCompositeUniqueField(
                    $invoice_no, $registration_no, $location
                    )
                    ->first();
            }else{
                $transaction = Transaction::findByCompositeUniqueTwoField(
                    $invoice_no, $registration_no
                    )
                    ->first();
            }
            $transaction->delete();
        }

        return response()->json($message);
    }

    public function restore($invoice_no, $registration_no, $location)
    {
        $transaction = Transaction::withTrashed()
            ->findByCompositeUniqueField($invoice_no, $registration_no, $location)
            ->first();
        $transaction->restore();
        
        return response()->json($transaction, 201);
    }

    public function plafon($personnel_no)
    {
        $usage = Transaction::usageOfPersonnel($personnel_no);
        $plafon = Plafon::all()->first();
        $balance = $plafon->amount - $usage;
        $eosUrl = config('medical.eos.url');
        $sslCert = config('ssl.cert');
        $position = $family = $structdisp = [];
        $datatest = [];

        $client = new Client([ 'base_uri' => $eosUrl, 'timeout'  => 3.0 ]);
        $response = $client->request('GET', 'structdisp/' . $personnel_no, ['verify' => $sslCert]);
        if ($response->getStatusCode() == 200)
            $structdisp = json_decode($response->getBody()->getContents());

        if(!empty($structdisp)) 
        {

            $client = new Client([ 'base_uri' => $eosUrl, 'timeout'  => 3.0 ]);
            $response = $client->request('GET', 'position/' . $personnel_no . '/last', ['verify' => $sslCert]);
            if ($response->getStatusCode() == 200)
                $position = json_decode($response->getBody()->getContents());

            $client = new Client([ 'base_uri' => $eosUrl, 'timeout'  => 3.0 ]);
            $response = $client->request('GET', 'family/' . $personnel_no, ['verify' => $sslCert]);
            if ($response->getStatusCode() == 200)
                $family = json_decode($response->getBody()->getContents());


            $client = new Client([ 'base_uri' => $eosUrl, 'timeout'  => 3.0 ]);
            $response = $client->request('GET', 'personalData/' . $personnel_no, ['verify' => $sslCert]);
            if ($response->getStatusCode() == 200)
                $personalData = json_decode($response->getBody()->getContents());
                
            $client = new Client([ 'base_uri' => $eosUrl, 'timeout'  => 3.0 ]);
            $response = $client->request('GET', 'personalAddress/' . $personnel_no .'/6', ['verify' => $sslCert]);
            if ($response->getStatusCode() == 200)
                $address = json_decode($response->getBody()->getContents());

            //  'structdisp' => $structdisp,
            return response()->json(collect([
                'balance' => $balance,
                'plafon' => $plafon->amount,
                'structdisp' => [
                    "personnel_no" => $structdisp->personnel_no,
                    "name" => $structdisp->name,
                    "esgrp" => $structdisp->esgrp,
                    "cost_ctr" => $structdisp->cost_ctr,
                    "position_name" => $structdisp->position_name,
                    "org_unit_name" => $structdisp->org_unit_name,
                    "place_of_birth" => $personalData->GBORT,
                    "date_of_birth" => $personalData->GBDAT,
                    "address" => $address->STRAS . ', ' . $address->LOCAT . ', ' . $address->ORT02 . ', ' . $address->T005U_BEZEI,
                    "phone" => $address->TELNR,
                ],
                'start_date' => $position->BEGDA,
                'end_date' => $position->ENDDA,
                'status' => $position->T501T_PTEXT,
                'families' => $family
            ]), 200);
        }
        else
        {
            return response()->json([]);
        }
    }
}