<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreInvoiceRequest;
use App\Invoice;
use App\Transaction;

class InvoiceController extends Controller
{

    public function index($offset)
    {
        $invoices = Invoice::paginate($offset);

        return $invoices;
    }

    public function store(StoreInvoiceRequest $request)
    {
        $invoice = Invoice::create($request->all());
                
        return response()->json($invoice, 201);
    }

    public function show($account_receivable)
    {
        $invoices = Invoice::findByAr($account_receivable)->get();

        return $invoices;
    }

    public function paginated($offset, $account_receivable)
    {
        $invoices = Invoice::findByAr($account_receivable)->paginate($offset);

        return $invoices;
    }

    public function update(Request $request)
    {
        // $data = $request->all();

        // foreach($data['invoice_no'] as $invoices)
        // {
        //     $invoice    = Invoice::where('invoice_no', $invoices[0])
        //                     ->where('account_receivable', $data['account_receivable'])
        //                     ->update([
        //                         'account_receivable' => $data['account_receivable'],
        //                         'invoice_no' => $invoices[0],
        //                         'correction' => $invoices[1],
        //                         'notes' => $invoices[2]
        //                     ]);
        // }

        // return response()->json($invoice, 201);
    }

    public function delete($invoice_no)
    {
        $invoice = Invoice::findByInvoice($invoice_no)->first();
        $invoice->delete();

        return response()->json(collect([
            'message' => 'sukses dihapus'
        ]), 204);
    }

    public function destroy($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function create()
    {
        //
    }
}
