<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'personnel_no' => 'required',
            'family_status' => 'required',
            'family_name' => 'required',
            'bpjs' => 'required',
            'examination_type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'registration_no' => [
                'required',
                Rule::unique('transactions')->where(function ($query) {
                    $query->where('invoice_no', $this->request->get('invoice_no'))
                        ->where('location', $this->request->get('location'));
                })
            ],
            'invoice_date' => 'required',
            'invoice_no' => [
                'required',
                Rule::unique('transactions')->where(function ($query) {
                    $query->where('location', $this->request->get('location'))
                        ->where('registration_no', $this->request->get('registration_no'));
                })
            ],
            'location' => [
                'required',
                Rule::unique('transactions')->where(function ($query) {
                    $query->where('invoice_no', $this->request->get('invoice_no'))
                        ->where('registration_no', $this->request->get('registration_no'));
                })
            ],
            'unit_service' => 'required',
            'doctor' => 'required',
            'cost' => 'required',
            'fee' => 'required',
            'cash' => 'required',
            'down_payment' => 'required',
            'bpjs_debt' => 'required',
            'company_debt' => 'required'
        ];
    }
}