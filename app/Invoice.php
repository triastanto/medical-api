<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_no',
        'account_receivable',
        'cost',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    public function scopeFindByInvoice($query, $i)
    {
        $query->where('invoice_no', $i);
    }

    public function scopeFindByAr($query, $i)
    {
        $query->where('account_receivable', $i);
    }
}