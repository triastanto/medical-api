<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [ 
        'personnel_no',
        'family_status',
        'family_name',
        'bpjs',
        'examination_type',
        'start_date',
        'end_date',
        'registration_no',
        'invoice_date',
        'invoice_no',
        'location',
        'unit_service',
        'doctor',
        'cost',
        'fee',
        'cash',
        'down_payment',
        'bpjs_debt',
        'company_debt',
        'updated_by'
    ];
    protected $hidden = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice', 'invoice_no', 'invoice_no');
    }

    public function scopeOfPersonnel($query, $p)
    {
        return $query->where('personnel_no', $p);
    }

    public function scopeOfRegisrationNo($query, $p)
    {
        return $query->where('registration_no', $p);
    }

    public function scopeFindByInvoice($query, $i)
    {
        return $query->where('invoice_no', $i);
    }

    public function scopeFindByCompositeUniqueField($query, $i, $t, $l)
    {
        return $query->where('invoice_no', $i)
            ->where('registration_no', $t)
            ->where('location', $l);
    }

    public function scopeFindByCompositeUniqueTwoField($query, $i, $t)
    {
        return $query->where('invoice_no', $i)->where('registration_no', $t);
    }

    public function scopeFamilyOnly($query)
    {
        return $query->where('family_status', 'family');
    }

    public function scopeEmployeeOnly($query)
    {
        return $query->where('family_status', 'employee');
    }

    public function scopeOutpatient($query)
    {
        return $query->where('examination_type', 'RJA1');
    }

    public function scopeCompanyDebtSum($query)
    {
        return $query->sum('company_debt');
    }

    public function scopeUsageOfPersonnel($query, $p)
    {
        return $query->ofPersonnel($p)
            ->familyOnly()
            ->Outpatient()
            ->companyDebtSum();
    }
}