<?php

return [
    "eos" => [
        "url" => env('MEDICAL_EOS_URL', 'https://portal.krakatausteel.com/eos/api/')
    ],
    "ssl" => [
        "cert" => env('SSL_CERT', 'C:\xampp\htdocs\medical-api\public\cacert.pem')
    ]
];