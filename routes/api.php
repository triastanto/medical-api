<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['prefix' => 'transactions', 'middleware' => ['auth:api']], function() {
    Route::get('', 'TransactionController@index');
    Route::get('/{registration_no}', 'TransactionController@show');
    Route::post('', 'TransactionController@store');
    Route::delete('/{invoice_no}/{registration_no}/{location?}', 'TransactionController@delete');
    Route::post('/restore/{invoice_no}/{registration_no}/{location}', 'TransactionController@restore');
    Route::get('/plafon/{personnel_no}', 'TransactionController@plafon');
});

Route::group(['prefix' => 'invoices', 'middleware' => ['auth:api']], function() {
    Route::get('/show/{account_receivable}', 'InvoiceController@show');
    Route::post('', 'InvoiceController@store');
    Route::put('/{invoice_no}/{account_receivable}', 'InvoiceController@update');
    Route::get('/paginated/{offset}/{account_receivable}', 'InvoiceController@paginated');
    Route::get('/{offset}', 'InvoiceController@index');
    Route::delete('/{invoice_no}', 'InvoiceController@delete');
});

Route::group(['prefix' => 'ars', 'middleware' => ['auth:api']], function() {
    Route::get('/show/{receipt}', 'ArController@show');
    Route::post('', 'ArController@store');
    Route::put('/{account_receivable}/{receipt}', 'ArController@update');
    Route::get('/paginated/{offset}/{receipt}', 'ArController@paginated');
    Route::get('/{offset}', 'ArController@index');
    Route::delete('/{account_receivable}', 'ArController@delete');
});
